<?php

$continents = [
    "Africa"        => ['Elephantidae', 'Panthera leo'],
    "Eurasia"       => ['Tigris', 'Ursus arctos'],
    "Australia"     => ['Troglodytes gorilla', 'Macropus'],
    "North America" => ['Panthera', 'Puma concolor'],
    "South America" => ['Crocodilia', 'Panthera pardus']
    ];

echo '<h1>1. Жестокое обращение с животными</h1>';

$two_word = [];
$continent = [];
$first_word = [];
$second_word = [];
foreach ($continents as $continents_key => $value_animals) {
    $continent[] = $continents_key;
    echo "<h2>$continents_key</h2>";
    echo $value_animals[0] . ', ' . $value_animals[1];
    foreach ($value_animals as $double) {
        $two = explode(" ", $double);
        if (count($two) == 2) {
            $two_word[] = $double;
            $first_word[] = $two[0];
            $second_word[] = $two[1];
        }
    }
}

echo '<h1>2. Новый массив с животными</h1>';
foreach ($two_word as $secondary) {
    echo "$secondary" . '</br>';
} 

shuffle($second_word);
echo '<h1>3. Фантазийные животные</h1>';
foreach ($second_word as $second) {
    $continental = array_shift($continent);
    $first = array_shift($first_word);
    echo "<h2>$continental</h2>";
    echo $first . ' ' . $second . '<br>';
}

?>